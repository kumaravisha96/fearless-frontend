window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const divTag = document.getElementById('loading-conference-spinner');
  const successTag = document.getElementById('success-message');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
        divTag.classList.add('d-none');

      // Here, remove the 'd-none' class from the select tag
        selectTag.classList.remove('d-none');
    }

    // create attendee
    const attendeeformTag = document.getElementById('create-attendee-form');
    attendeeformTag.addEventListener('submit', async event => {

        event.preventDefault();

        const attendeeformData = new FormData(attendeeformTag);
        const json = JSON.stringify(Object.fromEntries(attendeeformData));


        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': "application/json",
            },
        };

        const attendeeUrl = 'http://localhost:8001/api/attendees/';
      const response = await fetch(attendeeUrl, fetchConfig);





      // prevents congrats in the html to show up
      if (response.ok) {
        successTag.classList.remove('d-none');
        attendeeformTag.classList.add('d-none');
        attendeeformTag.reset();
      }
    })

  });
