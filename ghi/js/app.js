function createCard(name, description, pictureUrl, starts, ends, location) {
  return `

      <div class="card shadow p-3 mb-2 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${starts} - ${ends}
        </div>


  `;
}
var alertPlaceholder = document.getElementById("liveAlertPlaceholder");
function alert(message, type) {
  var wrapper = document.createElement("div");
  wrapper.innerHTML =
    '<div class="alert mb-0 alert-' +
    type +
    ' alert-dismissible" role="alert">' +
    message +
    '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';

  alertPlaceholder.append(wrapper);
}



window.addEventListener('DOMContentLoaded', async () => {
  const url = "http://localhost:8000/api/conferences/";
  const columns = document.querySelectorAll(".col-4");
  let colIndx = 0;


  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();


      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          // how to add subtitle
          const location = details.conference.location.name;

          // how to add dates
          const starts = new Date(details.conference.starts).toDateString();
          const ends = new Date(details.conference.ends).toDateString();

          // you have to keep adding to this
          const html = createCard(title, description, pictureUrl, starts, ends, location);

          // how to make columns side by side
          const column = columns[colIndx % 3];
          column.innerHTML += html;
          colIndx = (colIndx + 1) % 3;




        }
      }

    }
  } catch (e) {
    alert(`${e} Could not fetch the url`, "danger");


    console.error(e);

    // Figure out what to do if an error is raised
  }

});