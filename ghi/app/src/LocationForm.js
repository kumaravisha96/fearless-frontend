import React, { useEffect, useState } from 'react';

function Locationform(props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        // first create an empty data object, and then assign state values to the key names that the back end server is expecting
        const data = {};
        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;
        console.log(data);

        // POST code from new-location.js
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        // to clear the form: need to set the state to empty strings and use those values for the form elements
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setState('');
          }

      }
    // react hooks, change things in jsx after creating this

    // states is calling all of the states and state is referring to singular but you have to address both for it to work
    const [states, setStates] = useState([]);
    const [state, setState] = useState('')
    const handleStateChange = (event) => {
        // event.target.value property is the text that the user typed into the form."
        const value = event.target.value;
        setState(value)
    }
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [roomCount, setRoomCount] = useState(0);
    const handleRoomCount = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }
    const [city, setCity] = useState('');
    const handleCity = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    // const [states, setStates] = useState([]); gets all of the states
        const fetchData = async () => {
        // how to get actual data for states to show up
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states)
        }
      }
    //   react hooks
      useEffect(() => {
        fetchData();
      }, []);

    // JSX
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                {/* onChange updates the component state */}
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={roomCount} onChange={handleRoomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input value={city} onChange={handleCity} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select value={state} onChange={handleStateChange} required name="state" id="state" className="form-select">
                  <option value="">Choose a state</option>
                  {/* loop over */}
                  {states.map(state => {
                      return (
                          <option key={state.abbreviation} value={state.abbreviation}>
                              {state.name}
                          </option>
                      );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }



export default Locationform;
