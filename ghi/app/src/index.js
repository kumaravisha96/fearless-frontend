import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* runs the function and whatever JSX the function returns gets put there.
    That's how it ends up building the HTML for the page." */}
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadAttendees();



// <App attendees={data.attendees}/>, "that sets a variable named 'attendees'
// to the value inside the curly braces. Then,
// that variable becomes a property on the props parameter
// that gets passed into the function. "That's how you end up passing arguments
// to the React functions.They call them components, by the way,
// the functions in React that return JSX."